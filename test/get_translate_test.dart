import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:get_translate/get_translate.dart';

Future<void> main() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  group(
    'Translation',
    () {
      late Map<String, String> expectedFrTranslation;
      late Map<String, String> expectedEnTranslation;
      setUp(() {
        return Future(() async {
          expectedFrTranslation = {
            "basics.welcome": "Bonjour",
            "basics.back": "Retour",
            "basics.retry": "Réessayer",
            "basics_error.unexpected_error": "Une erreur est survenue",
            "basics_error.unauthorized_error":
                "Impossible d'accéder aux données",
            "booking_dashboard.title": "Mes Reservations",
            "booking_dashboard.filters.import": "Importer",
            "booking_dashboard.filters.export": "Exporter",
            "booking_dashboard.booking_card.date": "Date de la réservation",
            "booking_dashboard.booking_card.place": "Lieu",
          };
          expectedEnTranslation = {
            "basics.welcome": "Welcome",
            "basics.back": "Back",
            "basics.retry": "Retry",
            "basics_error.unexpected_error": "Unexpected Error",
            "basics_error.unauthorized_error": "Unauthorized Error",
            "booking_dashboard.title": "My bookings",
            "booking_dashboard.filters.import": "Import",
            "booking_dashboard.filters.export": "Export",
            "booking_dashboard.booking_card.date": "Booking time",
            "booking_dashboard.booking_card.place": "place",
          };
          await GetTranslate.initialize(
            locales: [
              Locale('fr', 'FR'),
              Locale('en', 'EN'),
              Locale('es', 'ES'),
            ],
            localeJsonPath: 'assets/translations/',
            fallbackLocale: Locale('fr', 'FR'),
          );
        });
      });
      test(
        'loadFallbackLocale',
        () {
          expectedFrTranslation.forEach((key, value) {
            expect(value, "$key".tr);
          });
        },
      );
      test(
        'changeLocaleInEn',
        () {
          GetTranslate.updateLocale(Locale('en', 'EN'));
          expectedEnTranslation.forEach((key, value) {
            expect(value, "$key".tr);
          });
        },
      );
      test(
        'changeLocaleInUnknown',
        () {
          GetTranslate.updateLocale(Locale('ch', 'CH'));
          expectedEnTranslation.forEach((key, value) {
            expect(value, "$key".tr);
          });
        },
      );
    },
  );
}

class FirstScreen extends StatelessWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(child: Text('FirstScreen'));
  }
}

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class ThirdScreen extends StatelessWidget {
  const ThirdScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}