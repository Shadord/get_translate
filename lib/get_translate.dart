library get_translate;

import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_navigation/src/root/internacionalization.dart';
import 'package:get/state_manager.dart';

/// A Calculator.
final GetTranslate = _Second();

class _Second extends _GetTranslateImpl {}

class _GetTranslateImpl extends GetxService with Translations {
  Future<void> initialize({
    required Locale fallbackLocale,
    required List<Locale> locales,
    required String localeJsonPath,
    Future? getterCurrentLocale,
    ValueChanged<Locale?>? onLocaleChanged,
  }) async {
    assert(locales.isNotEmpty);
    assert(localeJsonPath.isNotEmpty);
    assert(locales.contains(fallbackLocale));

    _fallbackLocale = fallbackLocale;
    _locales.addAll(locales);
    _localeJsonPath = localeJsonPath;
    _getterCurrentLocale = getterCurrentLocale;
    _onLocaleChanged = onLocaleChanged;
    _initialized = true;
    await _setDefaultLocale();
    _printLog("Initialized");
  }

  // ⎧⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎫
  // ⎨ VAR                         ⎬
  // ⎩⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎭

  /// Ensure it has been initialized
  bool _initialized = false;

  /// List of all locale supported by the application
  /// Note: [value] has to contain fallbackLocale
  /// and must not be empty
  List<Locale> _locales = [];

  /// Default locale
  /// be sure to have the [fallbackLocale.languageCode] in your localeJsonPath
  Locale _fallbackLocale = Locale('un', 'UN');

  /// Function that can be Future and be called before load fallbackLocale
  /// ex load a local save of the locale with Hive
  Future? _getterCurrentLocale;

  /// Function will be called each time locale has been changed
  /// return the locale if success, of null on fail
  ValueChanged<Locale?>? _onLocaleChanged;

  /// Path were locale json file are
  /// ex: assets/translations/
  String _localeJsonPath = "assets/translations/";

  final Rx<Locale> _locale = Rx<Locale>(Locale("fr"));
  final Map<String, Map<String, String>> _keys = {};

  // ⎧⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎫
  // ⎨ GETTERS                     ⎬
  // ⎩⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎭

  @override
  Map<String, Map<String, String>> get keys => _keys;

  // ⎧⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎫
  // ⎨ INIT                        ⎬
  // ⎩⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎭

  /// Override the default init function to load keys
  @override
  Future<void> onInit() async {
    super.onInit();
    await Get.putAsync(_getCurrentObject);
  }

  Future<_GetTranslateImpl> _getCurrentObject() async {
    return this;
  }

  /// Set all keys to empty value (to be defined)
  /// if(null) on updateLocale, selected key will be load from path
  /*void _initKeys() {
    for (final locale in _locales) {
      _keys[locale.toString()] = {};
    }
    _printLog("Key Initialized $_keys");
  }*/

  /// Check if a default locale is set by the getterCurrentLocale
  /// if(not) set the fallbackLocale
  Future<void> _setDefaultLocale() async {
    if (_getterCurrentLocale == null) {
      await updateLocale(_fallbackLocale);
      return;
    }
    final localeStr = await _getterCurrentLocale as String?;
    if (localeStr == null) {
      await updateLocale(_fallbackLocale);
      return;
    }
    try {
      await updateLocale(_parseLocale(localeStr));
      return;
    } on Exception catch (e) {
      print(e.toString());
      await updateLocale(_fallbackLocale);
      return;
    }
  }

  // ⎧⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎫
  // ⎨ UPDATE LOCALE               ⎬
  // ⎩⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎭

  /// Future Function that permit to update the locale of app
  /// [locale] must be in locales
  /// If [locale] is not in memory, it'll be loaded from path
  Future<Locale?> updateLocale(Locale locale) async {
    assert(_initialized);
    if (_checkKeys(locale)) {
      return _updateAppLocale(locale);
    }
    if (await _loadTranslationKey(locale) != null) {
      return _updateAppLocale(locale);
    }
    return null;
  }

  /// The key is sure to exist because preloaded in onInit function
  /// Check only if locale has already been loaded
  bool _checkKeys(Locale locale) => _keys.containsKey(locale.toString());

  Locale _updateAppLocale(Locale locale) {
    Get.updateLocale(locale);
    _locale.value = locale;
    if (_onLocaleChanged != null) {
      _onLocaleChanged!(locale);
    }
    _printLog("Locale Updated ${locale.toString()}");
    return locale;
  }

  Future<Locale?> _loadTranslationKey(Locale locale) async {
    try {
      final translationJson = await _getTranslationJsonOf(locale);
      final translationKey = _convertJsonToMap(translationJson);
      if (translationKey.isEmpty) {
        throw Exception("Error loading translationKey");
      }
      _keys[locale.toString()] = translationKey;
      Get.addTranslations(_keys);
      return locale;
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  // ⎧⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎫
  // ⎨ LOCALE LOADERS              ⎬
  // ⎩⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎭

  /// get the file of the desired locale decode to json => Map<String, dynamic>
  Future<Map<String, dynamic>> _getTranslationJsonOf(Locale locale) {
    return rootBundle
        .loadString('$_localeJsonPath${locale.languageCode}.json')
        .then(
      (value) => (jsonDecode(value) as dynamic) as Map<String, dynamic>,
      onError: (o, s) {
        throw Exception("Translation file not found $o -- $s");
      },
    );
  }

  /// Convert Map<String, dynamic> to Map<String, String>.
  /// The key correspond to the jsonPath
  Map<String, String> _convertJsonToMap(Map<String, dynamic> json) {
    return _recursiveFunction("", json);
  }

  /// _recursiveFunction convert a object Map<String, dynamic>
  /// into Map<String, String> on each sub children
  Map<String, String> _recursiveFunction(
    String currentPath,
    Map<String, dynamic> object,
  ) {
    var result = <String, String>{};
    var keys = object.keys;
    for (final key in keys) {
      if (object[key] is String) {
        if (result.containsKey(key)) {
          throw Exception("Translation file contain multiple identical keys");
        }
        result[currentPath.isEmpty ? key : "$currentPath.$key"] =
            object[key] as String;
      } else {
        result.addAll(_recursiveFunction(
          currentPath.isEmpty ? key : "$currentPath.$key",
          object[key] as Map<String, dynamic>,
        ));
      }
    }

    return result;
  }

  /// parseLocale take String like 'fr_FR', 'en_EN', 'es_ES'
  /// to convert it to a locale Locale(['fr', 'FR'])
  static Locale _parseLocale(String localeStr) {
    final codes = localeStr.split('_');
    if (codes.length == 2) {
      return Locale(codes[0], codes[1].toUpperCase());
    }
    throw Exception("Invalid lang format");
  }

  void _printLog(String message) =>
      developer.log(message, name: "GETX TRANSLATE");
}
