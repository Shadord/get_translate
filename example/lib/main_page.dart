import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_translate/get_translate.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              Text('unknown.trad'.tr),
              Text('basics.welcome'.tr),
              MaterialButton(
                child: Text("Change locale to EN"),
                onPressed: () {
                  GetTranslate.updateLocale(Locale('en', 'EN'));
                },
              ),
              MaterialButton(
                child: Text("Change locale to FR"),
                onPressed: () {
                  GetTranslate.updateLocale(Locale('fr', 'FR'));
                },
              ),
              MaterialButton(
                child: Text("Change locale to CH (Inconnue)"),
                onPressed: () {
                  GetTranslate.updateLocale(Locale('ch', 'CH'));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
