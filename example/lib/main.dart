import 'dart:async';
import 'package:example/app.dart';
import 'package:example/logger_utils.dart';
import 'package:flutter/material.dart';
import 'package:get_translate/get_translate.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  AppLogger.init();
  await GetTranslate.initialize(
    locales: [
      const Locale('fr', 'FR'),
      const Locale('en', 'EN'),
      const Locale('es', 'ES'),
    ],
    localeJsonPath: 'assets/translations/',
    fallbackLocale: const Locale('en', 'EN'),
  );

  runApp(App());
}
