import 'package:example/logger_utils.dart';
import 'package:example/main_page.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_translate/get_translate.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      builder: () => GetMaterialApp(
        title: "GetTranslate",
        enableLog: kReleaseMode,
        logWriterCallback: AppLogger.write,
        debugShowCheckedModeBanner: false,
        translations: GetTranslate,
        localizationsDelegates: const <LocalizationsDelegate>[
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        initialRoute: '/initial',
        getPages: [
          GetPage(
            name: '/initial',
            page: () => MainPage(),
          ),
        ],
      ),
    );
  }
}
